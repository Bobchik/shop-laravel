$(document).ready(function () {
   $('#product_img_edit').click(function () {
       $('#img_input').click();
   });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#product_img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}