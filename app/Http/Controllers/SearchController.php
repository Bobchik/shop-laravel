<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $product = Product::where('title', 'like',  "%{$request->keywords}%")->get();
        return response()->json($product);
    }
}
