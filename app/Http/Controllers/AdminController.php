<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('users.admin', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partials.product-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('imagePath');
        $path = 'img\shopping_cart\\';

        Product::create($request->all());
        if (!$image){
            Product::where('title', $request->title)->update([
                'imagePath' => $path . 'no-image.jpg'
            ]);
        }else{
            $file_name = $image->getClientOriginalName();
            Product::where('title', $request->title)->update([
                'imagePath' => $path . $file_name
            ]);
            $image->move($path, $file_name);
        }

        return redirect()->route('admin.create')->with('200', 'Product successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('shop.admin-product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $this->validate(request(), [
//
//        ]);
        $image = $request->file('image');
        $path = 'img\shopping_cart\\';

        if ($image) {
            $image->move($path, $image);
            $image = $image->getClientOriginalName();
            Product::find($id)->update([
                $request->all(),
                'imagePath' => $path.$image
            ]);
        }else{
            Product::find($id)->update($request->all());
        }

        return redirect()->route('admin.index')
            ->with('message', 'Product updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findOrFail($id)->delete();
        return back()->with('success', 'Product successfully deleted!');
    }
}
