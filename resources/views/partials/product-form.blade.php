@extends('layouts.master')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <form action="{{route('admin.store')}}" method="post" enctype="multipart/form-data">
        <div class="form-group col-sm-6 col-xs-4">
            <label for="title">Title</label>
            <input name="title" id="title" class="form-control" type="text" placeholder="Title">
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="price">Price</label>
            <input name="price" id="price" class="form-control" type="text" placeholder="Price">
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="shortDescription">Short description</label>
            <textarea name="shortDescription" id="shortDescription" class="form-control" placeholder="Short description"></textarea>
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="description">Description</label>
            <textarea name="description" id="description" class="form-control" placeholder="Description"></textarea>
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="author">Author</label>
            <input name="author" id="author" class="form-control" type="text" placeholder="Author">
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="pages">Number of pages</label>
            <input name="pages" id="pages" class="form-control" type="text" placeholder="Number of pages">
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="imagePath">Image</label>
            <input name="imagePath" id="imagePath" class="form-control" type="file">
        </div>
        <div class="form-group col-sm-6 col-xs-4">
            <label for="slug">Slug</label>
            <input name="slug" id="slug" class="form-control" type="text" placeholder="Slug">
        </div>
        {{csrf_field()}}
        <div class="text-center mt-4">
            <button class="btn btn-success" type="submit">Submit</button>
        </div>
    </form>
    @endsection