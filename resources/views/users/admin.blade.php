@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('admin.create') }}"> Orders information</a>
                <a class="btn btn-success" href="{{ route('admin.create') }}"> Add New Product</a>
            </div>
        </div>
    </div>
    <br>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Body</th>
            <th width="280px">Action</th>
        </tr>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->title}}</td>
                <td>{{ $product->shortDescription}}</td>
                <td>
                    <a class="btn btn-info" href="{{route('admin.show', [$product->id])}}">Show / Edit</a>
                    <form action="{{route('admin.destroy', [$product->id])}}" method="post" style="display: inline;">
                        <input type="hidden" name="_method" value="delete" />
                        <button class="btn btn-danger" type="submit">Delete</button>
                        {{ csrf_field() }}
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection