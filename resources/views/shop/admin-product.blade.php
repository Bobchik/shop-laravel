@extends('layouts.master')

@section('title')
    Laravel Shopping Cart
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <form action="{{ route('admin.update', [$product->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="patch"/>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="{{ asset('/img/shopping_cart/pencil.png') }}" id="product_img_edit" alt=""
                         style="width: 24px; margin-right: 0;">
                    <img src="{{asset($product->imagePath)}}" id="product_img" alt="...">
                    <input type="file" name="image" id="img_input" style="display: none;" onchange="readURL(this);">
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="caption">
                    <h3>Title</h3>
                    <input type="text" name="title" class="form-control" value="{{$product->title}}">
                    <div class="clearfix">
                    </div>
                    <label for="author">Author</label>
                    <input type="text" class="form-control" name="author" value="{{$product->author}}">
                    <label for="pages">Pages</label>
                    <input type="text" class="form-control" name="pages" value="{{$product->pages}}">
                    <label for="availability">Availability</label>
                    <input type="text" class="form-control" name="availability" value="{{$product->availability}}">
                    <label for="price">Price</label>
                    <input type="text" class="form-control" name="price" value="{{$product->price}}">

                </div>
            </div>
            <hr>
            <br>
            {{--<div class="row">--}}
                <textarea name="description" class="form-control description" rows="8" style="resize: none;">{{$product->description}}</textarea>
                <br>
                <br>
                <button type="submit" class="btn btn-primary" role="button">Update changes</button>
                <hr>
            {{--</div>--}}
        </form>
    </div>


@endsection