@extends('layouts.master')

@section('title')
    Laravel Shopping Cart
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
                <div id="charge-mesage" class="alert alert-success">
                    {{Session::get('success')}}
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div id="search">
                <search></search>
            </div>
        </div>
    </div>
    <br>
    @foreach($products->chunk(3) as $productChunk)
    <div class="row">
        @foreach($productChunk as $product)
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <a href="{{ route('product.show', ['slug' =>$product->slug]) }}"><img src="{{asset($product->imagePath)}}" alt="..."></a>
                <div class="caption">
                    <h3>{{$product->title}}</h3>
                    <p class="description">{{str_limit($product->shortDescription, 80)}}</p>
                    <div class="clearfix">
                        <div class="pull-left price">${{$product->price}}</div>
                        <a href="{{ route('product.addToCart', ['id' => $product->id])}}" class="btn btn-success pull-right" role="button">Add to Cart</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endforeach
    <div class="text-center">{{$products->links()}}</div>
@endsection
