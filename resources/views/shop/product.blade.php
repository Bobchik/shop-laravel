@extends('layouts.master')

@section('title')
    Laravel Shopping Cart
@endsection

@section('content')
    @foreach($product as $item)
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="{{'/'.$item['imagePath']}}" alt="...">
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="caption">
                    <h3>{{$item['title']}}</h3>
                    <div class="clearfix">

                    </div>
                    <ul class="list-group">

                        <li class="list-group-item">Author: <span class="pull-right">{{$item['author']}}</span></li>
                        <li class="list-group-item">Number of pages:<span class="pull-right">{{$item['pages']}}</span>
                        </li>
                        <li class="list-group-item">Availability: <span
                                    class="pull-right">{{$item['availability']}}</span></li>
                        <li class="list-group-item">Price:<span class="pull-right price"> ${{$item['price']}}</span>
                        </li>
                    </ul>
                    <a href="{{ route('product.addToCart', ['id' => $item['id']])}}" class="btn btn-success"
                       role="button">Add to Cart</a>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <p class="description">{{$item['description']}}</p>
        </div>
    @endforeach
@endsection